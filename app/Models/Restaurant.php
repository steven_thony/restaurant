<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Restaurant extends Eloquent {

	//
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'ms_restaurant';

	public $primaryKey='restaurant_id';

	protected $fillable = [
		'category_id','restaurant_name','restaurant_detail','restaurant_address','restaurant_lat','restaurant_long','status'
	];

	public function category(){
      return $this->belongsTo('App\Models\Category');
    }
}
