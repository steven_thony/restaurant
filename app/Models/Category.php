<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Category extends Eloquent {

	//
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'ms_category';

	public $primaryKey='category_id';

	protected $fillable = [
		'category_name','status'
	];

	public function restaurant(){
      return $this->hasMany('App\Models\Restaurant');
    }
}
