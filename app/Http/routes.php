<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

#-------------------
#------USER--------
#-------------------
Route::get('/','HomeController@index');
Route::get('/maps/{id}','HomeController@maps');

#-------------------
#------ADMIN--------
#-------------------
#LOGIN
//login show
Route::get('/hiddenadmin/login', array('uses' => 'AdminController@showLogin'));
//post login
Route::post('/hiddenadmin/login', array('uses' => 'AdminController@doLogin'));
//admin page show
Route::get('/admin', array('uses' => 'AdminController@index'));

#category
//view
Route::get('CategoryList','CategoryController@index');
//add
Route::get('load_addCategory', 'CategoryController@load_addCategory');
//create
Route::post('category_create', 'CategoryController@category_create');
//edit
Route::get('populate_editcategory/{id}/edit','CategoryController@populate_editcategory');
//update
Route::put('category_edits/{id}/edit', array('uses' => 'CategoryController@category_edits'));
//delete
Route::get('category_delete/{arrayid}', array('uses' => 'CategoryController@category_delete'));


#restaurant
//view
Route::get('RestaurantList','RestaurantController@index');
//add
Route::get('load_addRestaurant', 'RestaurantController@load_addRestaurant');
//create
Route::post('restaurant_create', 'RestaurantController@restaurant_create');
//edit
Route::get('populate_editrestaurant/{id}/edit','RestaurantController@populate_editrestaurant');
//update
Route::put('restaurant_edits/{id}/edit', array('uses' => 'RestaurantController@restaurant_edits'));
//delete
Route::get('restaurant_delete/{arrayid}', array('uses' => 'RestaurantController@restaurant_delete'));

//autocomplete lat long restaurant admin
Route::get('googleautocomplete/{address}','RestaurantController@autocomplete');

//admin logout
Route::get('hiddenadmin/logout', function()
{
    Auth::logout();
    return Redirect::to('hiddenadmin/login');
});