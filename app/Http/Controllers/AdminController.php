<?php
namespace App\Http\Controllers;

use Input;
use Auth;
use Redirect;
use DB;

use App\Http\Controllers\Controller;
use App\Http\Controllers\View;
use App\Models\Admin;

use Illuminate\Support\Facades\Validator;
use Illuminate\Cookies\CookieServiceProvider;
use Illuminate\Session\Middleware\StartSession;

use App\Http\Requests;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth',['except' => ['showLogin','doLogin']]);
        //$this->middleware('admin');
    }
    
    public function index()
    {
        return View('admin.index');
    }

    
    ## AUTH LOGIN
    public function showLogin()
    {
        return View('admin.login');
    }


   public function doLogin()
    {
            $email    = Input::get('email');
            $password = md5(Input::get('password'));

            $admin = count(Admin::where('email',$email)->where('password',$password)->first());

            // attempt to do the login
            if ($admin==1)
            {   //set session if login attempt successfull
                session_start();
                $admin_log_in = Admin::where('email','=',$email)->first();
                $_SESSION["admin_log_in"] =$admin_log_in->id;

                return View('admin.index',compact('admin_log_in'));
            }
            else
            {
                $error = 'Username and password is not match';
                return View('admin.login', compact('error'));
            }
    }

    
}
