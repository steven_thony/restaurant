<?php 
namespace App\Http\Controllers;
use View;
use DB;
use Session;

use App\Http\Requests;
use Illuminate\Http\Request;

use App\Models\Restaurant;
use App\Models\Contact;

class HomeController extends Controller {

	//show  home view
	public function index()
	{	
		$restaurant = Restaurant::where('restaurant_id','!=',1)->where('status','1')->get();
		$map = Restaurant::where('restaurant_id',1)->first();
		return View('home.view',compact('restaurant','map'));
	}


	//ajax map
	public function maps($id)
	{
		$map = Restaurant::where('restaurant_id',$id)->first();
		return View('home.map',compact('map'));
	}
}
