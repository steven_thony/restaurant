<?php 
namespace App\Http\Controllers;

use View;
use DB;
use Input;

use App\Models\Category;
use App\Models\Restaurant;

use Illuminate\Http\Request;

class RestaurantController extends Controller {

	public function index()
	{
		/*		
		=======================================
		Usage : show restaurant
		Table : ms_restaurant
		=======================================
		*/
		$restaurant = Restaurant::all();
		$count_restaurant = count($restaurant);
		return view('restaurant.admin',compact('restaurant','count_restaurant'));
	}

	public function load_addRestaurant()
	{
		/*		
		=======================================
		Usage : show add restaurant
		Table : ms_restaurant, ms_category
		=======================================
		*/

		$form_method_flag = 'add';
		$category = Category::where('status','=',1)->get();
		return View('restaurant.add',compact('form_method_flag','category'));
	}

	public function restaurant_create(Request $request)
	{
		/*		
		=======================================
		Usage : create restaurant
		Table : ms_restaurant
		=======================================
		*/
		
		$restaurant = new Restaurant();
		$restaurant->category_id = Input::get('category');
		$restaurant->restaurant_name = Input::get('name');
		$restaurant->restaurant_detail = Input::get('detail');
		$restaurant->restaurant_address = Input::get('address');
		$restaurant->restaurant_lat = Input::get('latitude');
		$restaurant->restaurant_long = Input::get('longitude');
		$restaurant->status = Input::get('status');
		$restaurant->save();

	    $restaurant = Restaurant::all();
		$count_restaurant = count($restaurant);
		return View('restaurant.admin',compact('restaurant','count_restaurant'));

		
	}

	public function populate_editrestaurant($id)
	{
		/*		
		=======================================
		Usage : show edit restaurant
		Table : ms_restaurant,ms_category
		=======================================
		*/
		$form_method_flag = 'edit';
		$restaurant = Restaurant::where('restaurant_id',$id)->first();
		$category = Category::where('status','=',1)->get();

		return View('restaurant.edit',compact('restaurant','form_method_flag','category'));
	}

	public function restaurant_edits($id)
	{
		/*		
		=======================================
		Usage : edit restaurant
		Table : ms_restaurant,ms_category
		=======================================
		*/

		$restaurant = Restaurant::where('restaurant_id',$id)->first();
		$restaurant->category_id = Input::get('category');
		$restaurant->restaurant_name = Input::get('name');
		$restaurant->restaurant_detail = Input::get('detail');
		$restaurant->restaurant_address = Input::get('address');
		$restaurant->restaurant_lat = Input::get('latitude');
		$restaurant->restaurant_long = Input::get('longitude');
		$restaurant->status = Input::get('status');
		$restaurant->save();

	    $restaurant = Restaurant::all();
		$count_restaurant = count($restaurant);
		return View('restaurant.admin',compact('restaurant','count_restaurant'));
	}

	public function restaurant_delete($arrayid)
	{
		/*		
		=======================================
		Usage : delete restaurant
		Table : ms_restaurant
		=======================================
		*/
		$arr = explode(",",$arrayid);
		DB::table('ms_restaurant')->whereIn('restaurant_id', $arr)->delete();

		$restaurant = Restaurant::all();
		$count_restaurant = count($restaurant);
		return View('restaurant.admin',compact('restaurant','count_restaurant'));
	}

	public function autocomplete($address)
	{
		// Address
		$address = addcslashes($address, '"\\/');

		// Get JSON results from this request
		
		$geo = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($address).'&sensor=false');

		// Convert the JSON to an array
		$geo = json_decode($geo, true);

		if ($geo['status'] == 'OK') {
		  // Get Lat & Long
		  $latitude = $geo['results'][0]['geometry']['location']['lat'];
		  $longitude = $geo['results'][0]['geometry']['location']['lng'];		 
		}else{
			$latitude = 'not found';
				$longitude = 'not found';
		}
		 $result = [$latitude,$longitude];

		return $result;
	}

}
