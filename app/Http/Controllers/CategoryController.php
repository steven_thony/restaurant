<?php 
namespace App\Http\Controllers;

use View;
use DB;
use Input;

use App\Http\Requests;

use App\Models\Category;
use App\Models\Product;

use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;

class CategoryController extends Controller {

	//admin
	public function index()
	{
		$category = Category::all();
		$count_category = count($category);
		return View('category.admin',compact('category','count_category'));
	}

	public function load_addCategory()
	{
		/*		
		=======================================
		Usage : show add category
		Table : ms_category
		=======================================
		*/

		$form_method_flag = 'add';
		return View('category.categoryform',compact('form_method_flag'));
	}

	public function category_create()
	{
		/*		
		=======================================
		Usage : add category
		Table : ms_category
		=======================================
		*/
		$category = new Category;
		$category->category_name = Input::get('name');
		$category->status = Input::get('status');
		$category->save();

		
		$category = Category::all();
		$count_category = count($category);

		return View('category.admin',compact('category','count_category'));
	}

	public function populate_editcategory($id)
	{
		/*		
		=======================================
		Usage : show edit category
		Table : ms_category
		=======================================
		*/

		$category = Category::where('category_id',$id)->first();
		$form_method_flag = 'edit';

		return View('category.categoryform',compact('category','form_method_flag'));
	}

	public function category_edits($id)
	{
		/*		
		=======================================
		Usage : edit category
		Table : ms_category
		=======================================
		*/
		$category = Category::where('category_id',$id)->first();
		$category->category_name = Input::get('name');
		$category->status = Input::get('status');
		$category->save();
		$lastID = Category::get()->last();

		
		$category = Category::all();
		$count_category = count($category);

		return View('category.admin',compact('category','count_category'));
	}


	public function category_delete($arrayid)
	{
		/*		
		=======================================
		Usage : delete category
		Table : ms_category
		=======================================
		*/
		$arr = explode(",",$arrayid);
		DB::table('ms_category')->whereIn('category_id', $arr)->delete();

		$category = Category::all();
		$count_category = count($category);
		return View('category.admin',compact('category','count_category'));
	}

}
