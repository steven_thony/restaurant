<div class="ajaxify-wrapper">
<div class="box">
      <div class="box-header">

        <h3 class="box-title">{{$count_category}} Category</h3>
      </div><!-- /.box-header -->

      <button class="add-btn btn btn-success btn-lg" style="padding:8px 25px;"><i class="glyphicon glyphicon-plus-sign"  aria-hidden="true"></i><br/><span style="font-size:0.7em;"> Add</span></button>

      <button  class="btn btn-danger btn-lg" data-toggle="modal" data-target="#delete-modal" style="padding:8px 20px;"><i class="glyphicon glyphicon-trash"  aria-hidden="true"></i><br/><span style="font-size:0.7em;">Delete</span></button>

      <div class="box-body">
      
      

    <!--<img class="loading_main" src="{{ asset('assets/fix/bar120.gif') }}">-->
      <table data-source="#" data-filter="#filter_table" id="standardcrud_datatable" class="table table-bordered table-striped">

        <thead>
          <tr>  
              <th> Action </th>
              <th> Id   </th>
              <th> Name </th>
              <th> Active </th>
          </tr> 
        </thead>
        <tbody align="center">
        @foreach($category as $category)
              <tr>
                  <td>
                  <input id="checkbox" type="checkbox" name="checkbox_id[]" value="{{ $category->category_id }}">
                  <button class="btn btn-default edit-btn" id="edit{{ $category->category_id }}" data-toggle="modal" data-target="#edit-modal">
                  <i class="glyphicon glyphicon-edit"></i>
                  </button>
                  </td>
                  <td>{{$category->category_id}}</td>
                   <td>{{$category->category_name}}</td>
                 <td> {{$category->status ? 'Active' : 'Non Active'}} </td>
              </tr>
        @endforeach
        </tbody>
      </table>

    </div>
</div>


  <!-- DATA TABLES SCRIPT -->

  <!-- COMPOSE MESSAGE MODAL -->
<div class="modal fade" id="compose-modal" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog">
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <h4 class="modal-title"><i class="fa fa-envelope-o"></i> Add New Category</h4>
              </div>

              <iframe name="upload-frame" id="upload-frame" style="display:none;"></iframe>
              <div id="addform-content">
                <!-- load form via Ajax -->
                <form action="{{ url() }}/category_create" id="createForm" method="post" target="upload-frame" enctype="multipart/form-data">
              </div> <!-- /#addform-content -->
          </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->

  <!-- EDIT MODAL -->
  <div class="modal fade" id="edit-modal" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="false">
      <div class="modal-dialog">
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <h4 class="modal-title"><i class="fa fa-envelope-o"></i> Edit Category</h4>
              </div>

              <iframe name="upload-frame-2" id="upload-frame-2" style="display:none;"></iframe>
              <div id="editform-content">

                <!-- load form via Ajax -->
               
              </div> <!-- /#editform-content -->
          </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->


  <!-- DELETE MODAL -->
    <div class="modal fade" id="delete-modal">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">DELETE</h4>
          </div>
          <div class="modal-body">
            <p>Are you sure to delete these records? &hellip;</p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
            <button type="button" class="delete-btn btn btn-primary">Delete</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

</div>

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs-3.3.7/jq-2.2.4/dt-1.10.15/datatables.min.css"/>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script> 
<script type="text/javascript" src="https://cdn.datatables.net/v/bs-3.3.7/jq-2.2.4/dt-1.10.15/datatables.min.js"></script>
<script type="text/javascript">
$(".loading_main").hide();
    $("#standardcrud_datatable").dataTable();
    // load Add Modal Form
    $(".add-btn").on('click',function(){

      $('#compose-modal').modal({ show: true,backdrop: false });
      $.ajax({
        url: '{{ URL() }}/load_addCategory',
        beforeSend: function () {
          $(".loading_main").show();
        },
        success: function (message) {
          //alert(message);
          $(".loading_main").hide();
          $("#addform-content").html(message);
          $('#compose-modal').modal({backdrop: false,hide:true});
        }
        
      });
    });


    //populate Edit Modal Form
    $("#standardcrud_datatable").on('click','.edit-btn',function(){
      var getid = $(this).attr('id');
      var url = getid.replace(/^edit+/, "");
      $.ajax({
        url: '{{ URL() }}/populate_editcategory/'+url+"/edit",
        beforeSend: function () {
          $(".loading_main").show();
        },
        success: function (message) {
          //alert(message);
          $(".loading_main").hide();
          $("#editform-content").html(message);
        }
      });
    });

    $(".modal").on('click','.submit-btn1',function(){

      $('#compose-modal').modal('hide');
      $('#edit-modal').modal('hide');

      $.ajax({
        url: '{{ URL() }}/CategoryList',
        beforeSend: function () {
          $(".loading_main").show();
        },
        success: function (message) {
          //alert(message);
          $(".loading_main").hide();
          //$(".conten t").remove();
          $(".content").html(message);
        },
        complete:function(){
        $.ajax({
        url: '{{ URL() }}/CategoryList',
        beforeSend: function () {
          $(".loading_main").show();
        },
        success: function (message) {
          //alert(message);
          $(".loading_main").hide();
          $(".content").html(message);
          }
      });
        }
      });

    });


    //validasi
    $(".modal").on('click','.submit-btn',function(){
      var valid = 1;
      var count= 0;
      var error=[];
      var text='';



      if($('#input-name').val() == '')
      {
        document.getElementById('input-name').style.borderColor = "red";
        error[count] = 'Name must be input';
        valid = 0;
        count++;
      }
      else
      {
        document.getElementById('input-name').style.borderColor = "green"; 
      }

      for (i = 0; i < count; i++) {
       text += error[i] + "\n";
      }
      
      if(valid == 0){
        alert(text);
        return false;
      }

      $('#compose-modal').modal('hide');
      $('#edit-modal').modal('hide');

      $.ajax({
        url: '{{ URL() }}/CategoryList',
        beforeSend: function () {
          $(".loading_main").show();
        },
        success: function (message) {
          //alert(message);
          $(".loading_main").hide();
          //$(".content").remove();
          $(".content").html(message);
        },
        complete:function(){
           $.ajax({

            url: '{{ URL() }}/CategoryList',
            beforeSend: function () {

              $(".loading_main").show();
            },
            success: function (message) {
                //alert(message);
              $(".loading_main").hide();
                //$(".conten t").remove();
              $(".content").html(message);
            }
          });
         }
      });
    });

$(".btn-danger").click(function(){
  var valid=1;
  if($('input#checkbox').is(':checked')){
  
            valid;
          }
        else{
            alert("You didn't check it!");
            valid = 0;
    }

    if(valid == 0){
        return false;
      }
});
  
    
    $(".delete-btn").on('click',function () {
      $('#delete-modal').modal('hide');

        var searchIDs = $('input[name="checkbox_id[]"]:checked').map(function(){
          return $(this).val();
        }).get();

        $.ajax({
          url: '{{ URL() }}/category_delete/'+searchIDs,
          beforeSend: function () {
            $(".loading_main").show();
          },
          success: function (message) {
            //alert(message);
            $(".loading_main").hide();
            $(".content").html(message);
          },
          complete:function()
          {
          $.ajax({
        url: '{{ URL() }}/CategoryList',
        beforeSend: function () {
          $(".loading_main").show();
        },
        success: function (message) {
          //alert(message);
          $(".loading_main").hide();
          $(".content").html(message);
        }
      });
          }
        });
    });
</script>