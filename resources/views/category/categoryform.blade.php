<!-- use this for create -->
@if($form_method_flag=='add')
<form action="{{ url() }}/category_create" id="createForm" method="post" target="upload-frame" enctype="multipart/form-data">
@endif
<!-- use this for update -->
@if($form_method_flag=='edit')
<form action="{{ url() }}/category_edits/{{ $category->category_id }}/edit" id="editForm" method="post" target="upload-frame-2" enctype="multipart/form-data">
@endif
    <!-- only use this for update -->

    @if($form_method_flag=='edit')
    <input type="hidden" name="_method" value="PUT">
    @endif
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div class="modal-body">
        

        <div class="form-group">
            <div class="input-group">
                <span class="input-group-addon">Name</span>
                <input name="name" id="input-name" type="text" class="form-control" value="<?php if(isset($category)) echo $category->category_name;?>">
            </div>
        </div>


        <div class="form-group">
            <div class="input-group">
                <span class="input-group-addon">Status</span>
                <select name ="status" id="input-category" class="form-control">
                    <option name="status" value="2">--Choose--</option>
                    <option name="status" value="1" <?php if(isset($category->status) == '1') echo  "selected";else echo ''; ?>>Active</option>
                    <option name="status" value="0" <?php if(isset($category->status) == '0') echo  "selected";else echo ''; ?>>Non Active</option>
                </select>
            </div>
        </div>

    </div>

    <div class="modal-footer clearfix">

        <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Discard</button>

        @if($form_method_flag=='add')
        <button type="submit" class="submit-btn btn btn-primary pull-left"><i class="fa fa-envelope"></i> Add</button>
        @endif
        @if($form_method_flag=='edit')
        <button type="submit" class="submit-btn1 btn btn-primary pull-left"><i class="fa fa-envelope"></i> Update</button>
        @endif
    </div>
</form>