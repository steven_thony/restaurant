 <!-- use this for create -->
  @if($form_method_flag=='add')
  <form action="{{ url() }}/restaurant_create" id="createForm" method="post" target="upload-frame" enctype="multipart/form-data">
  @endif
  <!-- use this for update -->
  @if($form_method_flag=='edit')
  <form action="{{ url() }}/restaurant_edits/{{ $restaurant->restaurant_id }}/edit" id="editForm" method="post" target="upload-frame-2" enctype="multipart/form-data">
  @endif
  <!-- only use this for update -->

  @if($form_method_flag=='edit')
  <input type="hidden" name="_method" value="PUT">
  @endif
  <input type="hidden" name="_token" value="{{ csrf_token() }}">
	
  <div class="form-group">
    <div class="input-group">
        <span class="input-group-addon">Category</span>
        <select class="form-control" name="category" id="input-category" >
          <option value="">-- Choose --</option>
            @foreach($category as $category)
            <option name="category" value="{{ $category->category_id }}"<?php if(isset($restaurant)){ if(strpos($restaurant->category_id,strval($category->category_id) ) !== FALSE ) echo 'selected'; } ?>>{{ $category->category_name }}
            @endforeach
        </select>
    </div>
  </div>

  <div class="form-group">
    <div class="input-group">
        <span class="input-group-addon">Name</span>
        <input name="name" id="input-name" type="text" class="form-control" value="<?php if(isset($restaurant)) echo $restaurant->restaurant_name;?>">
    </div>
  </div>



  <div class="form-group">
    <div class="input-group">
        <span class="input-group-addon">Description</span>
        <textarea id="edit" name="detail" class="form-control"><?php if(isset($restaurant)) echo $restaurant->restaurant_detail;?></textarea>
    </div>
  </div>

  <div class="form-group">
    <div class="input-group">
        <span class="input-group-addon">Latitude</span>
        <input name="latitude" id="input-latitude" type="text" class="form-control" value="<?php if(isset($restaurant)) echo $restaurant->restaurant_lat;?>">
    </div>
  </div>

  <div class="form-group">
    <div class="input-group">
        <span class="input-group-addon">Longitude</span>
        <input name="longitude" id="input-longitude" type="text" class="form-control" value="<?php if(isset($restaurant)) echo $restaurant->restaurant_long;?>">
    </div>
  </div>

  <div class="form-group">
    <div class="input-group">
        <span class="input-group-addon">Address</span>
        <textarea id="autocomplete" name="address" class="form-control"><?php if(isset($restaurant)) echo $restaurant->restaurant_address;?></textarea>
    </div>
  </div>

   <div class="form-group">
   <iframe width="480" height="300" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.it/maps?q=singapore&output=embed" id="googleMaps"></iframe>

       <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA1f3d88vszQ0x5RExjKsWez8r5e516ELE&libraries=geometry,places"></script>
          <script type="text/javascript">
          function initialize() {
              var input = document.getElementById('autocomplete');
              var options = {componentRestrictions: {country: 'ID'}};
                           
              new google.maps.places.Autocomplete(input, options);
          }
          </script>
    </div>

    <div class="form-group">
    <div class="input-group">
        <span class="input-group-addon">Status</span>
        <select class="form-control" name="status" id="input-status">
          <option value="2">-- Choose --</option>
            <option name="status" value="1">Active</option>
            <option name="status" value="0">Non Active</option>
        </select>
    </div>
  </div>


    

  @if($form_method_flag=='add')
  <button type="submit" class="submit-btn btn btn-primary pull-left"><i class="fa fa-envelope"></i> Add</button>
  @endif
  @if($form_method_flag=='edit')
  <button type="submit" class="submit-btn1 btn btn-primary pull-left"><i class="fa fa-envelope"></i> Update</button>
  @endif
	</form>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<!-- Initialize the editor. -->
  <script>
    $('#autocomplete').on('change paste keyup',function(){
    var src = $(this).val();
   $('#googleMaps').attr('src','https://maps.google.it/maps?q='+ src +'&output=embed');

    $.ajax({
          url: '{{ URL() }}/googleautocomplete/'+src,
          beforeSend: function () {
            //$(".loading_main").show();
          },
          success: function (message) {
            $('#input-latitude').val(message[0]);
            $('#input-longitude').val(message[1]);
          }
        });

  });
    //validasi
    $(".submit-btn").on('click',function(){
      var valid = 1;
      var count= 0;
      var error=[];
      var text='';

      if($('#input-name').val() == '')
      {
        document.getElementById('input-name').style.borderColor = "red";
        error[count] = 'Name must be input';
        valid = 0;
        count++;
      }
      else
      {
        document.getElementById('input-name').style.borderColor = "green"; 
      }

      if($('#input-category').val() == '')
      {
        document.getElementById('input-category').style.borderColor = "red";
        error[count] = 'Category must be input';
        valid = 0;
        count++;
      }
      else
      {
        document.getElementById('input-category').style.borderColor = "green"; 
      }

      if($('#edit').val() == '')
      {
        document.getElementById('edit').style.borderColor = "red";
        error[count] = 'Description must be input';
        valid = 0;
        count++;
      }
      else
      {
        document.getElementById('edit').style.borderColor = "green"; 
      }

      if($('#autocomplete').val() == '')
      {
        document.getElementById('autocomplete').style.borderColor = "red";
        error[count] = 'Address must be input';
        valid = 0;
        count++;
      }
      else
      {
        document.getElementById('autocomplete').style.borderColor = "green"; 
      }

      if($('#input-latitude').val() == '')
      {
        document.getElementById('input-latitude').style.borderColor = "red";
        error[count] = 'Latitude must be input';
        valid = 0;
        count++;
      }
      else
      {
        document.getElementById('input-latitude').style.borderColor = "green"; 
      }

      if($('#input-longitude').val() == '')
      {
        document.getElementById('input-longitude').style.borderColor = "red";
        error[count] = 'Longitude must be input';
        valid = 0;
        count++;
      }
      else
      {
        document.getElementById('input-longitude').style.borderColor = "green"; 
      }

      if($('#input-status').val() == '2')
      {
        document.getElementById('input-status').style.borderColor = "red";
        error[count] = 'Status must be choose';
        valid = 0;
        count++;
      }
      else
      {
        document.getElementById('input-status').style.borderColor = "green"; 
      }

      for (i = 0; i < count; i++) {
       text += error[i] + "\n";
      }
      
      if(valid == 0){
        alert(text);
        return false;
      }
    });


  $('#createForm').submit(function(evt) {
      evt.preventDefault();
        var formData = new FormData(this);
        $.ajax({
          type: 'POST',
          url: "{{ URL::to('/restaurant_create') }}",
          data:formData,
          cache:false,
          contentType: false,
          processData: false,
          beforeSend: function () {
          $('.loading_main').show();
          },
          success: function (message) {
          $('.loading_main').show();
          },
          complete:function()
          {
          $.ajax({
          url: '{{ URL() }}/RestaurantList',
          beforeSend: function () {
           $('.loading_main').show();
          },
          success: function (message) {
          $(".content").html(message);
          }
        });
          }
        });
    });

  </script>