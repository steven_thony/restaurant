<div class="ajaxify-wrapper">
<div class="box">
      <div class="box-header">

        <h3 class="box-title">{{$count_restaurant}} Restaurant</h3>
      </div><!-- /.box-header -->

      <button class="view-btn btn btn-success btn-lg" style="padding:8px 25px;"><i class="fa fa-play"></i><br/><span style="font-size:0.7em;"> View</span></button>

      <button class="add-btn btn btn-success btn-lg" style="padding:8px 25px;"><i class="glyphicon glyphicon-plus-sign"  aria-hidden="true"></i><br/><span style="font-size:0.7em;"> Add</span></button>

      <button  class="btn btn-danger btn-lg" data-toggle="modal" data-target="#delete-modal" style="padding:8px 20px;"><i class="glyphicon glyphicon-trash"  aria-hidden="true"></i><br/><span style="font-size:0.7em;">Delete</span></button>

      <div class="box-body">
      
      
    <div class="restaurant-content">
    <!--<img class="loading_main" src="{{ asset('assets/fix/bar120.gif') }}">-->
      <table data-source="#" data-filter="#filter_table" id="standardcrud_datatable" class="table table-bordered table-striped">

        <thead>
          <tr>  
              <th> Action </th>
              <th> Id   </th>
              <th> Category </th>
              <th> Restaurant Name </th>
              <th> Description </th>
              <th> Address </th>
              <th> Latitude</th>
              <th> Longitude</th>
              <th> Active </th>
          </tr> 
        </thead>
        <tbody align="center">
        @foreach($restaurant as $restaurant)
              <tr>
                  <td>
                  <input id="checkbox" type="checkbox" name="checkbox_id[]" value="{{ $restaurant->restaurant_id }}">
                  <button class="btn btn-default edit-btn" id="edit{{ $restaurant->restaurant_id }}">
                  <i class="glyphicon glyphicon-edit"></i>
                  </button>
                  </td>
                  <td>{{$restaurant->restaurant_id}}</td>
                  <td> {{$restaurant->category->category_name}} </td>
                  <td>{{$restaurant->restaurant_name}}</td>
                  <td><?php $jumlahkarakter=50;
                  $cetak = substr($restaurant->restaurant_detail, 0, $jumlahkarakter);
                  ?>{{$cetak}}</td>
                  <td>{{$restaurant->restaurant_address}}</td>
                  <td>{{$restaurant->restaurant_lat}}</td>
                  <td>{{$restaurant->restaurant_long}}</td>
                  <td>{{$restaurant->status ? 'Active' : 'Non Active'}}</td>
                 
              </tr>
        @endforeach
        </tbody>
      </table>
      </div>

    </div>
</div>


  <!-- DATA TABLES SCRIPT -->

  <!-- DELETE MODAL -->
    <div class="modal fade" id="delete-modal">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">DELETE</h4>
          </div>
          <div class="modal-body">
            <p>Are you sure to delete these records? &hellip;</p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
            <button type="button" class="delete-btn btn btn-primary">Delete</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

</div>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs-3.3.7/jq-2.2.4/dt-1.10.15/datatables.min.css"/>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script> 
<script type="text/javascript" src="https://cdn.datatables.net/v/bs-3.3.7/jq-2.2.4/dt-1.10.15/datatables.min.js"></script>

<script type="text/javascript">
$(function(){
$("#standardcrud_datatable").dataTable();
});
</script>
<script>
$(".loading_main").hide();
    // load Add Modal Form
    $(".add-btn").on('click',function(){

      //$('#compose-modal').modal({ show: true,backdrop: false });
      $.ajax({
        url: '{{ URL() }}/load_addRestaurant',
        beforeSend: function () {
          $(".loading_main").show();
        },
        success: function (message) {
          //alert(message);
          $(".loading_main").hide();
          $('.restaurant-content').html(message);
          //$("#addform-content").html(message);
          //$('#compose-modal').modal({backdrop: false,hide:true});
        }
        
      });
    });

    $(".view-btn").on('click',function(){
      $.ajax({
        url: '{{ URL() }}/RestaurantList',
        beforeSend: function () {
          $(".loading_main").show();
        },
        success: function (message) {
          $(".loading_main").hide();
          $('.content').html(message);
        }
        
      });
    });


    //populate Edit Modal Form
    $("#standardcrud_datatable").on('click','.edit-btn',function(){
      var getid = $(this).attr('id');
      var url = getid.replace(/^edit+/, "");
      $.ajax({
        url: '{{ URL() }}/populate_editrestaurant/'+url+"/edit",
        beforeSend: function () {
          $(".loading_main").show();
        },
        success: function (message) {
          //alert(message);
          $(".loading_main").hide();
          $('.restaurant-content').html(message);
          //$("#editform-content").html(message);
        }
      });
    });


$(".btn-danger").click(function(){
  var valid=1;
  if($('input#checkbox').is(':checked')){
  
            valid;
          }
        else{
            alert("You didn't check it!");
            valid = 0;
    }

    if(valid == 0){
        return false;
      }
});
  
    
    $(".delete-btn").on('click',function () {
      $('#delete-modal').modal('hide');

        var searchIDs = $('input[name="checkbox_id[]"]:checked').map(function(){
          return $(this).val();
        }).get();

        $.ajax({
          url: '{{ URL() }}/restaurant_delete/'+searchIDs,
          beforeSend: function () {
            $(".loading_main").show();
          },
          success: function (message) {
            //alert(message);
            $(".loading_main").hide();
            $(".content").html(message);
          },
          complete:function()
          {
          $.ajax({
        url: '{{ URL() }}/RestaurantList',
        beforeSend: function () {
          $(".loading_main").show();
        },
        success: function (message) {
          //alert(message);
          $(".loading_main").hide();
          $(".content").html(message);
        }
      });
          }
        });
    });
</script>