<!DOCTYPE html>
<html>
     <div class="loading"></div>
    <head>
        <meta charset="UTF-8">
        <title>Restaurant</title>
        <link href="{{ URL::asset('public/css/normalize.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ URL::asset('public/css/main.css') }}" rel="stylesheet"  media="screen" type="text/css" />
        <link href='http://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Playball' rel='stylesheet' type='text/css'>
        <link href="{{ URL::asset('public/css/bootstrap.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ URL::asset('public/css/style-portfolio.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ URL::asset('public/css/picto-foundry-food.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ URL::asset('public/css/jquery-ui.css') }}" rel="stylesheet" type="text/css" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="{{ URL::asset('public/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" />
        <link rel="icon" href="favicon-1.ico" type="image/x-icon">
    </head>

    <body>
        <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
            <div class="container">
                <div class="row">
                <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="#">Restaurant</a>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav main-nav  clear navbar-right ">
                            <li><a class="navactive color_animation" href="#top">WELCOME</a></li>
                            <li><a class="color_animation" href="#story">ABOUT</a></li>
                            <li><a class="color_animation" href="#pricing">PRICING</a></li>
                            <li><a class="color_animation" href="#beer">BEER</a></li>
                            <li><a class="color_animation" href="#bread">BREAD</a></li>
                            <li><a class="color_animation" href="#featured">FEATURED</a></li>
                             <li><a class="color_animation" href="#contact">MAPS</a></li>
                        </ul>
                    </div><!-- /.navbar-collapse -->
                </div>
            </div><!-- /.container-fluid -->
        </nav>
         
        <div id="top" class="starter_container bg">
            <div class="follow_container">
                <div class="col-md-6 col-md-offset-3">
                    <h2 class="top-title"> Restaurant</h2>
                    <h2 class="white second-title">" Best in the city "</h2>
                    <hr>
                </div>
            </div>
        </div>

        <!-- ============ About Us ============= -->

        <section id="story" class="description_content">
            <div class="text-content container">
                <div class="col-md-6">
                    <h1>About us</h1>
                    <div class="fa fa-cutlery fa-2x"></div>
                    <p class="desc-text">Restaurant is a place for simplicity. Good food, good beer, and good service. Simple is the name of the game, and we’re good at finding it in all the right places, even in your dining experience. We’re a small group from Denver, Colorado who make simple food possible. Come join us and see what simplicity tastes like.</p>
                </div>
                <div class="col-md-6">
                    <div class="img-section">
                       <img src="{{asset('public/images/kabob.jpg')}}" width="250" height="220">
                       <img src="{{asset('public/images/limes.jpg')}}" width="250" height="220">
                       <div class="img-section-space"></div>
                       <img src="{{asset('public/images/radish.jpg')}}"  width="250" height="220">
                       <img src="{{asset('public/images/corn.jpg')}}"  width="250" height="220">
                   </div>
                </div>
            </div>
        </section>


       <!-- ============ Pricing  ============= -->


        <section id ="pricing" class="description_content">
             <div class="pricing background_content">
                <h1><span>Affordable</span> pricing</h1>
             </div>
            <div class="text-content container"> 
                <div class="container">
                    <div class="row">
                        <div id="w">
                            <ul id="filter-list" class="clearfix">
                                <li class="filter" data-filter="all">All</li>
                                <li class="filter" data-filter="breakfast">Breakfast</li>
                                <li class="filter" data-filter="special">Special</li>
                                <li class="filter" data-filter="desert">Desert</li>
                                <li class="filter" data-filter="dinner">Dinner</li>
                            </ul><!-- @end #filter-list -->    
                            <ul id="portfolio">
                                <li class="item breakfast"><img src="{{asset('public/images/food_icon01.jpg')}}" alt="Food" >
                                    <h2 class="white">$20</h2>
                                </li>

                                <li class="item dinner special"><img src="{{asset('public/images/food_icon02.jpg')}}" alt="Food" >
                                    <h2 class="white">$20</h2>
                                </li>
                                <li class="item dinner breakfast"><img src="{{asset('public/images/food_icon03.jpg')}}" alt="Food" >
                                    <h2 class="white">$18</h2>
                                </li>
                                <li class="item special"><img src="{{asset('public/images/food_icon04.jpg')}}" alt="Food" >
                                    <h2 class="white">$15</h2>
                                </li>
                                <li class="item dinner"><img src="{{asset('public/images/food_icon05.jpg')}}" alt="Food" >
                                    <h2 class="white">$20</h2>
                                </li>
                                <li class="item special"><img src="{{asset('public/images/food_icon06.jpg')}}" alt="Food" >
                                    <h2 class="white">$22</h2>
                                </li>
                                <li class="item desert"><img src="{{asset('public/images/food_icon07.jpg')}}" alt="Food" >
                                    <h2 class="white">$32</h2>
                                </li>
                                <li class="item desert breakfast"><img src="{{asset('public/images/food_icon08.jpg')}}" alt="Food" >
                                    <h2 class="white">$38</h2>
                                </li>
                            </ul><!-- @end #portfolio -->
                        </div><!-- @end #w -->
                    </div>
                </div>
            </div>  
        </section>


        <!-- ============ Our Beer  ============= -->


        <section id ="beer" class="description_content">
            <div  class="beer background_content">
                <h1>Great <span>Place</span> to enjoy</h1>
            </div>
            <div class="text-content container"> 
                <div class="col-md-5">
                   <div class="img-section">
                       <img src="{{asset('public/images/beer_spec.jpg')}}" width="100%">
                   </div>
                </div>
                <br>
                <div class="col-md-6 col-md-offset-1">
                    <h1>OUR BEER</h1>
                    <div class="icon-beer fa-2x"></div>
                    <p class="desc-text">Here at Restaurant we’re all about the love of beer. New and bold flavors enter our doors every week, and we can’t help but show them off. While we enjoy the classics, we’re always passionate about discovering something new, so stop by and experience our craft at its best.</p>
                </div>
            </div>
        </section>


       <!-- ============ Our Bread  ============= -->


        <section id="bread" class=" description_content">
            <div  class="bread background_content">
                <h1>Our Breakfast <span>Menu</span></h1>
            </div>
            <div class="text-content container"> 
                <div class="col-md-6">
                    <h1>OUR BREAD</h1>
                    <div class="icon-bread fa-2x"></div>
                    <p class="desc-text">We love the smell of fresh baked bread. Each loaf is handmade at the crack of dawn, using only the simplest of ingredients to bring out smells and flavors that beckon the whole block. Stop by anytime and experience simplicity at its finest.</p>
                </div>
                <div class="col-md-6">
                    <img src="{{asset('public/images/bread1.jpg')}}" width="260" alt="Bread">
                    <img src="{{asset('public/images/bread1.jpg')}}" width="260" alt="Bread">
                </div>
            </div>
        </section>


        
        <!-- ============ Featured Dish  ============= -->

        <section id="featured" class="description_content">
            <div  class="featured background_content">
                <h1>Our Featured Dishes <span>Menu</span></h1>
            </div>
            <div class="text-content container"> 
                <div class="col-md-6">
                    <h1>Have a look to our dishes!</h1>
                    <div class="icon-hotdog fa-2x"></div>
                    <p class="desc-text">Each food is handmade at the crack of dawn, using only the simplest of ingredients to bring out smells and flavors that beckon the whole block. Stop by anytime and experience simplicity at its finest.</p>
                </div>
                <div class="col-md-6">
                    <ul class="image_box_story2">
                        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                            <!-- Indicators -->
                            <ol class="carousel-indicators">
                                <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                                <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                                <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                            </ol>

                            <!-- Wrapper for slides -->
                            <div class="carousel-inner">
                                <div class="item active">
                                    <img src="{{asset('public/images/slider1.jpg')}}"  alt="...">
                                    <div class="carousel-caption">
                                        
                                    </div>
                                </div>
                                <div class="item">
                                    <img src="{{asset('public/images/slider2.jpg')}}" alt="...">
                                    <div class="carousel-caption">
                                        
                                    </div>
                                </div>
                                <div class="item">
                                    <img src="{{asset('public/images/slider3.jpg')}}" alt="...">
                                    <div class="carousel-caption">
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </ul>
                </div>
            </div>
        </section>

        

        <!-- ============ Social Section  ============= -->
      
        <section class="social_connect">
            <div class="text-content container"> 
                <div class="col-md-6">
                    <span class="social_heading">FOLLOW</span>
                    <ul class="social_icons">
                        <li><a class="icon-twitter color_animation" href="#" target="_blank"></a></li>
                        <li><a class="icon-github color_animation" href="#" target="_blank"></a></li>
                        <li><a class="icon-linkedin color_animation" href="#" target="_blank"></a></li>
                        <li><a class="icon-mail color_animation" href="#"></a></li>
                    </ul>
                </div>
                <div class="col-md-4">
                    <span class="social_heading">OR DIAL</span>
                    <span class="social_info"><a class="color_animation" href="tel:883-335-6524">(941) 883-335-6524</a></span>
                </div>
            </div>
        </section>

        <!-- ============ Contact Section  ============= -->

        <section id="contact" style="background-color: white">
            <div class="text-content container"> 
                <div class="col-md-12">
                    <span class="social_heading">Our Outlet</span>
                    <ul class="social_icons">
                    <li><a href="#"  class="restaurant navactive" value="{{$map->restaurant_id}}"><font color="black">{{$map->restaurant_name}}</font></a></li>
                    @foreach($restaurant as $restaurant)
                    <li><a href="#" class="restaurant" value="{{$restaurant->restaurant_id}}"><font color="black">{{$restaurant->restaurant_name}}</font></a></li>
                    @endforeach
                    </ul>
                </div>
               
            </div>
        </section>


        <section id="contact" style="margin-bottom: 40px;">
        <div id="maps">
            <div id="map" style="width:100%;height:450px;border:0;margin-bottom: 30px;"></div>

         <!-- COMPOSE MESSAGE MODAL -->
        <div class="modal fade compose-modal" tabindex="-1" role="dialog" aria-hidden="true">
              <div class="modal-dialog">
                  <div class="modal-content">
                      <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                          <h4 class="modal-title"><i class="fa fa-envelope-o"></i> Directions</h4>
                      </div>

                      <iframe name="upload-frame" id="upload-frame" style="display:none;"></iframe>
                      <div class="addform-content">
                       <div id="panel{{$map->restaurant_id}}" class="panel"></div>
                      </div> <!-- /#addform-content -->
                  </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
          </div><!-- /.modal -->

        </div>
        <button type="submit" name="direction" class="form-btn1 direction">Show Direction</button> </span>
        </section>



        <!-- ============ Footer Section  ============= -->

        <footer class="sub_footer">
            <div class="container">
                <div class="col-md-4"><p class="sub-footer-text text-center">&copy; Restaurant 2014, Theme by <a href="https://themewagon.com/">ThemeWagon</a></p></div>
                <div class="col-md-4"><p class="sub-footer-text text-center">Back to <a href="#top">TOP</a></p>
                </div>
                <div class="col-md-4"><p class="sub-footer-text text-center">Built With Care By <a href="#" target="_blank">Us</a></p></div>
            </div>
        </footer>

        <meta name="csrf-token" content="{{ csrf_token() }}">
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script src="{{ URL::asset('public/js/bootstrap.min.js') }}" type="text/javascript"></script>
        <script src="{{ URL::asset('public/js/jquery.mixitup.min.js') }}" type="text/javascript"></script>
        <script src="{{ URL::asset('public/js/main.js') }}" type="text/javascript"></script>

    </body>
    
</html>

<script type="text/javascript">
$(".form-btn1.direction").on('click',function(){
      $('.modal.fade.compose-modal').modal();
    });
$(function() {
    
    // Asynchronously Load the map API 
    var script = document.createElement('script');
    script.src = "http://maps.google.com/maps/api/js?key=AIzaSyA1f3d88vszQ0x5RExjKsWez8r5e516ELE&sensor=false&callback=initialize";
    document.body.appendChild(script);
});

function initialize() {
var geocoder;
var map;
var directionsDisplay;
var directionsService = new google.maps.DirectionsService();

navigator.geolocation.getCurrentPosition(function(position){ 

  var locations = [
   ['You are here',position.coords.latitude,position.coords.longitude],['<?php echo $map->restaurant_name; ?>', '<?php echo $map->restaurant_lat; ?>','<?php echo $map->restaurant_long; ?>']
  ];
 
  directionsDisplay = new google.maps.DirectionsRenderer();


  var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 10,
    center: new google.maps.LatLng(-33.92, 151.25),
    mapTypeId: google.maps.MapTypeId.ROADMAP
  });
  directionsDisplay.setMap(map);
  directionsDisplay.setPanel(document.getElementById('panel{{$map->restaurant_id}}'));

  var infowindow = new google.maps.InfoWindow();

  var marker, i;
  var request = {
    travelMode: google.maps.TravelMode.DRIVING
  };
  for (i = 0; i < locations.length; i++) {
    marker = new google.maps.Marker({
      position: new google.maps.LatLng(locations[i][1], locations[i][2]),
      map: map
    });

    google.maps.event.addListener(marker, 'click', (function(marker, i) {
      return function() {
        infowindow.setContent(locations[i][0]);
        infowindow.open(map, marker);
      }
    })(marker, i));
    if (i == 0) request.origin = marker.getPosition();
    else if (i == locations.length - 1) request.destination = marker.getPosition();
    else {
      if (!request.waypoints) request.waypoints = [];
      request.waypoints.push({
        location: marker.getPosition(),
        stopover: true
      });
    }

  }
  directionsService.route(request, function(result, status) {
    if (status == google.maps.DirectionsStatus.OK) {
      directionsDisplay.setDirections(result);
    }
  }); 
});
}

 $('.restaurant').click(function(){
    $(this).addClass('navactive');
    var id = $(this).attr('value');
       $.ajax({
        url: "{{ URL::to('maps') }}/"+id,
        beforeSend: function () {
          $(".loading").show();
        },
        success: function (message) {
          $(".loading").hide();
          $("#map").html(message);
          
        },
        complete : function(){
           //$(".addform-content").empty();
           $(".panel").remove();
           $('.addform-content').parent().append('<div id="panel'+id+'" class="panel"></div>');
        }
      });
    });
</script>