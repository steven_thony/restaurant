<div id="map" style="width:100%;height:450px;border:0"></div>


<script>
$(function() {
    // Asynchronously Load the map API 
    var script = document.createElement('script');
    script.src = "http://maps.google.com/maps/api/js?key=AIzaSyA1f3d88vszQ0x5RExjKsWez8r5e516ELE&sensor=false&callback=initialize";
    document.body.appendChild(script);
});

function initialize() {
var geocoder;
var map;
var directionsDisplay;
var directionsService = new google.maps.DirectionsService();

navigator.geolocation.getCurrentPosition(function(position){ 
  var locations = [
    ['You are here',position.coords.latitude,position.coords.longitude],['<?php echo $map->restaurant_name; ?>', '<?php echo $map->restaurant_lat; ?>','<?php echo $map->restaurant_long; ?>']
  ];
  
  directionsDisplay = new google.maps.DirectionsRenderer();


  var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 10,
    center: new google.maps.LatLng(-33.92, 151.25),
    mapTypeId: google.maps.MapTypeId.ROADMAP
  });
  directionsDisplay.setMap(map);
   directionsDisplay.setPanel(document.getElementById('panel{{$map->restaurant_id}}'));
  var infowindow = new google.maps.InfoWindow();

  var marker, i;
  var request = {
    travelMode: google.maps.TravelMode.DRIVING
  };
  for (i = 0; i < locations.length; i++) {
    marker = new google.maps.Marker({
      position: new google.maps.LatLng(locations[i][1], locations[i][2]),
      map: map
    });

    google.maps.event.addListener(marker, 'click', (function(marker, i) {
      return function() {
        infowindow.setContent(locations[i][0]);
        infowindow.open(map, marker);
      }
    })(marker, i));
    if (i == 0) request.origin = marker.getPosition();
    else if (i == locations.length - 1) request.destination = marker.getPosition();
    else {
      if (!request.waypoints) request.waypoints = [];
      request.waypoints.push({
        location: marker.getPosition(),
        stopover: true
      });
    }

  }
  directionsService.route(request, function(result, status) {
    if (status == google.maps.DirectionsStatus.OK) {
      directionsDisplay.setDirections(result);
    }
  });
  });
}


</script>